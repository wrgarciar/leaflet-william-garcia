var mymap = L.map('mapid');
mymap.setView([4.652742, -74.153579], 13);

/*L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);*/

var provider = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});

provider.addTo(mymap);

var yo = L.marker([4.652742, -74.153579]).addTo(mymap);
var cuñada = L.marker([4.621441, -74.199118]).addTo(mymap);
var mama = L.marker([4.593415, -74.205398]).addTo(mymap);
var hermano = L.marker([4.751755, -74.101573]).addTo(mymap);

var circuloyo = L.circle([4.652742, -74.153579], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

var circulocuñada = L.circle([4.621441, -74.199118], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

var circulomama = L.circle([4.593415, -74.205398], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

var circulohermano = L.circle([4.751755, -74.101573], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

var popup = L.popup()
    .setLatLng([4.652742, -74.153579])
    .setContent("Hola Ing. Paulo, aquí vivo yo!")
    .openOn(mymap);

/*var popup2 = L.popup()
    .setLatLng([4.621441, -74.199118])
    .setContent("Aquí mi cuñada!")
    .openOn(mymap);*/

var polygon = L.polygon([
        [4.652742, -74.153579],
        [4.621441, -74.199118],
        [4.593415, -74.205398],
        [4.751755, -74.101573]
    ]).addTo(mymap);